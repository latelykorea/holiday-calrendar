import dotenv from 'dotenv';
import axios from 'axios';
import file from './record_dt.json'
import {Log} from './logger.js'
import fs from 'fs';
import moment from 'moment-timezone'

const logger = Log({serviceName: 'collector'})
dotenv.config();

export const Holiday = function () {
  this._fromDate = file.record_date;
}

Holiday.prototype.getRows = async function () {
  const http = axios.create({
    baseURL: 'http://ws.sellerhub.co.kr'
  })
  http.defaults.headers['AuthorizeCode'] = 'bdb56eae34073e792cbf78d1d260acee'
  const fromDate = this._time(this._fromDate);
  const toDate = this._time(moment().tz('Asia/Seoul'));
  const str = `select IDX, UNAME, TEAM, GROUP_TYPE, START_DATE, END_DATE, USING_DAY, MESSAGE, GRANTOR_1_STATE, GRANTOR_1_MESSAGE, GRANTOR_2_STATE, GRANTOR_2_MESSAGE, GRANTOR_3_STATE, GRANTOR_3_MESSAGE, STATE, FORMAT(CDATE, 'yyyy-MM-dd HH:mm:ss') as C from EADMIN_PAYMENT_LIST where STATE !=0 and GROUP_TYPE !='관리자입력' and CDATE between '${fromDate}' and '${toDate}' order by CDATE asc`
  logger.info(`Search Holiday Period [${fromDate}] ~ [${toDate}]`)
  const resp = await http.get(`/Database/Select/List?SqlQuery=${encodeURI(str)}`)
  return resp.data.RowLists;
}
Holiday.prototype.record = async function(record_date, cb) {
  try {
    const data = {record_date: this._time(record_date)};
    fs.writeFileSync('./record_dt.json', JSON.stringify(data, null, 0), cb)
  } catch (e) {
    cb(e)
  }
}

Holiday.prototype._time = function (dt) {
  return moment(dt).format('YYYY-MM-DD HH:mm:ss')
}
