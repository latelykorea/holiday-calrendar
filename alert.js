import axios from 'axios';
import querystring from 'querystring';

export const Alert = function (msg) {
  let url = 'https://gw.lately.co.kr:15001/webapi/entry.cgi?api=SYNO.Chat.External&method=incoming&version=2&token=%22GtRSQjnWS55WDlVdSAF1nl3nKGrw41bKTiP4faC35HzAkmAIzRLe6r7TTjiDa3KX%22';
  const makePayload = (err) => {
    return querystring.stringify ({'payload': JSON.stringify ({'text': `[Error occurred in Holiday Calender] ${err}`})})
  }
  let headers = {'Content-type': 'application/x-www-form-urlencoded'};
  axios.post (url, makePayload(msg), {headers});
}
