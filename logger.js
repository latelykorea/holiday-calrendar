import winston from 'winston'
import DailyRotateFile from 'winston-daily-rotate-file'

const logger = ({serviceName}) => winston.createLogger({
  level: 'debug',
  format: winston.format.combine(
    winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
    winston.format.splat(),
    winston.format.printf(p => {
      return `[${p.timestamp}] \t [${p.service}] \t [${p.level}] \t ${JSON.stringify(p.message)} `
    })
  ),
  defaultMeta: { service: serviceName},
  transports: [
    new winston.transports.Console({level: 'debug'}),
    new DailyRotateFile({filename: './logs/error.log', level: 'error'}),
    new DailyRotateFile({filename: './logs/calendar.log', level:'info'})
  ]
})

export {logger as Log}
