import { Client } from "@notionhq/client"
import dotenv from 'dotenv'
dotenv.config();
import {Holiday} from './holidays.js'
import {Log} from './logger.js'
import moment from 'moment-timezone'
import {Alert} from "./alert.js";
const logger = Log({serviceName: 'writer'})

const notion = new Client({ auth: process.env.NOTION_KEY })
const database_id = process.env.NOTION_DATABASE_ID
const search = 1001947
const holiday = new Holiday();

const map = async (rows) => {
  const _STATE = {
    '1': '승인 요청',
    '2': '승인 완료',
    '9': '승인 거절'
  }
  return rows.map(row => {

    return {
      'idx': row.Row0,
      'name': row.Row1,
      'department': row.Row2,
      'type': row.Row3,
      'startAt': row.Row4.replace(/(\d{4})(\d{2})(\d{2})/g, '$1-$2-$3'),
      'endAt': row.Row5.replace(/(\d{4})(\d{2})(\d{2})/g, '$1-$2-$3'),
      'period': row.Row6,
      'state': _STATE[row.Row14],
      'recordDt': row.Row15
    }
  })
}

const isExistData = async (row) => {
  const data = await notion.databases.query({
    database_id,
    filter: {
      and: [
        {
          property: 'idx',
          text: {
            equals: row.idx
          }
        }
      ]
    }
  })
  logger.info(`idx: ${row.idx} exist: ${data.results.length > 0}`);
  return data.results.length > 0;
}

const write = async (row) => {
  try {
    let args = {
      parent: {database_id},
      properties: {
        idx: {
          rich_text: [
            {
              text: {
                content: row.idx
              }
            }
          ]
        },
        name: {
          title: [
            {
              text: {
                content: row.name
              }
            }
          ]
        },
        Department: {
          rich_text: [
            {
              text: {
                content: row.department
              }
            }
          ]
        },
        Date: {
          date: {
            start: moment(row.startAt).format('YYYY-MM-DD'),
            end: moment(row.endAt).format('YYYY-MM-DD')
          }
        },
        days: {
          number: parseInt(row.period)
        },
        Type: {
          multi_select: [{
            name: row.type
          }]
        },
        State: {
          select: {
            name: row.state
          }
        },
      }
    };
    // console.log(JSON.stringify(args,null, 2));
    const response = await notion.pages.create(args)
    logger.info(`Update notion table row idx : ${row.idx}`);
  } catch (e) {
    throw new Error(e)
  }
}

const exec = async (rows) => {
  logger.info(`Update started with a number of ${rows.length}.`)
  let count = 0;
  for (const row of rows) {
    if (!await isExistData(row)){
      await write(row)
      count = count + 1;
    }
    await holiday.record(row.recordDt, function (err)  {
      if (err) {
        throw new Error(err);
      }
    })

  }
  logger.info(`Update ${count}, Passed ${rows.length - count}`)
}

holiday.getRows()
  .then(map)
  .then(exec)
  .catch(err => {
    logger.error(err);
    Alert(err);
  });

