FROM node:14
WORKDIR /app
COPY package*.json ./
RUN npm install
ENV NOTION_KEY secret_iB8gYcHSQxqEuhS9wHORh0sxx6AubNvOtSAGeqWmsJw
ENV NOTION_DATABASE_ID 76c49e7df0df4939bc1db3922d5164df

COPY . .

CMD ["npm", "start"]